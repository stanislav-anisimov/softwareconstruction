package com.sanisimov.myrpg;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sanisimov.myrpg.characters.Monster;
import com.sanisimov.myrpg.characters.Player;
import com.sanisimov.myrpg.items.FlaskFactory;
import com.sanisimov.myrpg.util.Direction;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.util.TextureStorage;


public class MyRPG extends ApplicationAdapter
{
	SpriteBatch batch;
	Texture pen;
	Zone level;
	OrthographicCamera cam;
	Position fieldCenter;
	BitmapFont font;
	FPSLogger logger;

	@Override
	public void create()
	{
		batch = new SpriteBatch();
		pen = new Texture(Gdx.files.internal("pen.png"));
		TextureStorage.init();

		int camHeight = 500;
		int camWidth = 500;
		cam = new OrthographicCamera(camWidth, camHeight * Gdx.graphics.getHeight() / Gdx.graphics.getWidth());
		cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
		fieldCenter = new Position((int) cam.viewportWidth / 3, (int) cam.viewportHeight / 2);

		level = new Zone(10, 10);
		level.setCellSize(30);
		level.setLineThickness(cam.viewportWidth / 500f);

		final Player player = new Player(new Position(5, 5));
		player.stats.str = 30;
		level.setPlayer(player);
		level.add(player);
		level.add(new Monster(new Position(1, 2)));
		level.add(new Monster(new Position(3, 6)));
		level.add(new Monster(new Position(7, 2)));
		level.add(new Monster(new Position(4, 9)));
		level.add(new Monster(new Position(5, 10)));
		level.add(new Monster(new Position(7, 3)));
		level.add(new Monster(new Position(1, 8)));
		level.add(FlaskFactory.randomFlask(new Position(0,0)));
		level.add(FlaskFactory.randomFlask(new Position(1,0)));
		level.add(FlaskFactory.randomFlask(new Position(2,0)));

		level.setBottomLeft(fieldCenter);

		Gdx.input.setInputProcessor(new InputAdapter()
		{
			@Override
			public boolean keyDown(int keycode)
			{
				switch (keycode)
				{
					case Input.Keys.W:
						level.moveCharacter(level.getPlayer(), Direction.NORTH);
						break;
					case Input.Keys.A:
						level.moveCharacter(level.getPlayer(), Direction.WEST);
						break;
					case Input.Keys.S:
						level.moveCharacter(level.getPlayer(), Direction.SOUTH);
						break;
					case Input.Keys.D:
						level.moveCharacter(level.getPlayer(), Direction.EAST);
						break;
					case Input.Keys.NUM_1:
						player.use(1);
						break;
					case Input.Keys.NUM_2:
						player.use(2);
						break;
					case Input.Keys.NUM_3:
						player.use(3);
						break;
					case Input.Keys.NUM_4:
						player.use(4);
						break;

				}
				return true;
			}

			@Override
			public boolean touchDown(int x, int y, int pointer, int button)
			{
				Vector3 wordCoords = cam.unproject(new Vector3(x, y, 0));
				level.onClick(wordCoords, fieldCenter);
				return true;
			}
		});

		font = new BitmapFont();

		logger = new FPSLogger();

		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		batch.begin();
		level.render(batch, pen, fieldCenter, font);
		batch.end();

		logger.log();
	}

	@Override
	public void dispose()
	{
		batch.dispose();
		pen.dispose();
	}
}
