package com.sanisimov.myrpg.characters;

import com.badlogic.gdx.graphics.Texture;
import com.sanisimov.myrpg.*;
import com.sanisimov.myrpg.items.ExpFactory;
import com.sanisimov.myrpg.items.FlaskFactory;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.util.TextureStorage;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sanis on 21.09.2017.
 */
public class Monster extends Character
{
	public Monster(Position pos)
	{
		this.position = pos;

		idleAnimation = TextureStorage.getAnimation("monster");
	}

	@Override
	public Position getPosition()
	{
		return position;
	}

	@Override
	public void setPosition(Position pos)
	{
		this.position = pos;
	}

	@Override
	public boolean displayHealth()
	{
		return currentHp < totalHp;
	}

	@Override
	public float getHealthPercent()
	{
		return (float) currentHp / totalHp;
	}

	@Override
	public void onAttacked(Character enemy)
	{
		Random rng = new Random();
		if (rng.nextInt(100) > calculateEvasionChance(this.stats))
		{
			this.damage(calculateDamage(enemy.stats));
			if (this.currentHp == 0)
			{
				this.onDeath();
				return;
			}
		}
		this.attackStart(enemy);
	}

	@Override
	public Texture getTexture()
	{
		return texture;
	}

	@Override
	public void attackStart(IPlaceable target)
	{
		//target.onAttacked(this);
	}

	@Override
	protected void onDeath()
	{
		placeExp();
		placeLoot();
		this.map.remove(this);
		_finalize();
	}

	private void placeLoot()
	{
		Random rng = new Random();
		if (rng.nextInt(100) < 30)
		{
			int flaskCount = rng.nextInt(3) + 1;
			ArrayList<Position> neighbours = this.position.getNeighbours();
			for (Position pos : neighbours)
			{
				if (flaskCount > 0 && map.add(FlaskFactory.randomFlask(pos)))
				{
					flaskCount--;
				}
			}
		}
	}

	private void placeExp(){
		Random rng = new Random();
		int orbCount = rng.nextInt(2) + 1;
		ArrayList<Position> neighbours = this.position.getNeighbours();
		for (Position pos : neighbours)
		{
			if (orbCount > 0 && map.add(ExpFactory.getOrb(pos)))
			{
				orbCount--;
			}
		}
	}
}
