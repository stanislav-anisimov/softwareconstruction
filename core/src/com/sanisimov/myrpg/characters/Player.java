package com.sanisimov.myrpg.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Timer;
import com.sanisimov.myrpg.IPlaceable;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.util.TextureStorage;

import java.util.Random;

/**
 * Created by sanis on 21.09.2017.
 */
public class Player extends Character
{

	public Player(Position position)
	{
		this.position = position;
		idleAnimation = TextureStorage.getAnimation("player");
		attackAnimation = TextureStorage.getAnimation("player_attack");
	}

	@Override
	public Position getPosition()
	{
		return position;
	}

	@Override
	public void setPosition(Position pos)
	{
		this.position = pos;
	}

	@Override
	public boolean displayHealth()
	{
		return true;
	}

	@Override
	public float getHealthPercent()
	{
		return (float)currentHp / totalHp;
	}

	@Override
	public void onAttacked(Character enemy)
	{
		Random rng = new Random();
		if(rng.nextInt(100) > calculateEvasionChance(this.stats)){
			this.damage(calculateDamage(enemy.stats));
			if(this.currentHp==0){
				this.onDeath();
				return;
			}
		}
	}

	@Override
	public Texture getTexture()
	{
		return texture;
	}


	@Override
	public void attackStart(final IPlaceable target)
	{
		if(isAttacking){
			return;
		}
		isAttacking = true;
		final Character ch = this;
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				ch.isAttacking = false;
				target.onAttacked(ch);
			}
		}, 1/(float)this.stats.dex, 0, 0);
	}

	public void use(int slot){
		slot--;
		if(this.inventory.size()>slot){
			inventory.get(slot).use(this);
			inventory.remove(slot);
		}
	}

	@Override
	protected void onDeath()
	{
		_finalize();
	}
}
