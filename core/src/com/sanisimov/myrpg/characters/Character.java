package com.sanisimov.myrpg.characters;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.sanisimov.myrpg.IPlaceable;
import com.sanisimov.myrpg.items.Item;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.Zone;

import java.util.ArrayList;

public abstract class Character implements IPlaceable
{
	protected Position position = null;
	public int currentHp = 100;
	public int totalHp = 100;
	protected Texture texture = null;
	protected Animation<Texture> idleAnimation;
	protected Animation<Texture> attackAnimation;
	protected Zone map = null;
	protected ArrayList<Item> inventory = new ArrayList<Item>();
	protected boolean isAttacking = false;

	public boolean canMove(){
		return !isAttacking;
	}

	protected void _finalize(){
		if(texture!=null){
			texture.dispose();
		}
	}
	@Override
	public Zone getMap()
	{
		return map;
	}

	@Override
	public void setMap(Zone map)
	{
		this.map = map;
	}

	public class Level{
		public int level = 1;
		public int currentExp = 0;
		public int totalExp = 1000;
		public void addExp(int amount){
			currentExp+=amount;
			while(currentExp>totalExp){
				currentExp-=totalExp;
				level+=1;
			}
		}
		public float getExpPercent(){
			return (float)currentExp/totalExp;
		}
	}

	public class Stats
	{
		public Level level = new Level();
		public int str = 1;
		public int dex = 1;
	}

	public Stats stats = new Stats();

	public void addExp(int amount){
		this.stats.level.addExp(amount);
	}

	public void setLevel(int level){
		this.stats.level.level = level;
	}


	public abstract void attackStart(IPlaceable target);

	protected int calculateDamage(Stats stats)
	{
		return stats.str * stats.level.level;
	}

	protected int calculateEvasionChance(Stats stats)
	{
		return stats.dex * stats.level.level;
	}

	public void damage(int damage)
	{
		this.currentHp -= damage;
		this.currentHp = this.currentHp < 0 ? 0 : this.currentHp;
	}

	public void heal(int heal){
		this.currentHp += heal;
		this.currentHp = this.currentHp > this.totalHp ? this.totalHp : this.currentHp;
	}


	protected abstract void onDeath();

	public Texture getAnimationKeyFrame(float animationStateTime){
		if(isAttacking){
			return attackAnimation.getKeyFrame(animationStateTime, false);
		}
		else{
			return idleAnimation.getKeyFrame(animationStateTime, true);
		}
	}

	public void pickUp(Item item){
		inventory.add(item);
	}

	public ArrayList<Item> getInventory(){
		return inventory;
	}
}
