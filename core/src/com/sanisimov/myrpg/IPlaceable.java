package com.sanisimov.myrpg;


import com.badlogic.gdx.graphics.Texture;
import com.sanisimov.myrpg.characters.Character;
import com.sanisimov.myrpg.util.Position;

public interface IPlaceable
{
	public void setMap(Zone map);

	public Zone getMap();

	public Position getPosition();

	public void setPosition(Position pos);

	public boolean displayHealth();

	public float getHealthPercent();

	public void onAttacked(Character enemy);

	Texture getTexture();

	Texture getAnimationKeyFrame(float animationStateTime);
}
