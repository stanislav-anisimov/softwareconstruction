package com.sanisimov.myrpg.items;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.sanisimov.myrpg.IPlaceable;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.util.TextureStorage;
import com.sanisimov.myrpg.Zone;
import com.sanisimov.myrpg.characters.Character;

/**
 * Created by sanis on 24.09.2017.
 */
public class ExpOrb implements IPlaceable
{

	private Zone map;
	private Position position;
	private Animation<Texture> animation;
	private int amount = 100;

	ExpOrb(int amount){
		animation = TextureStorage.getAnimation("orb_exp");
		this.amount = amount;
	}

	@Override
	public void setMap(Zone map)
	{
		this.map = map;
	}

	@Override
	public Zone getMap()
	{
		return this.map;
	}

	@Override
	public Position getPosition()
	{
		return position;
	}

	@Override
	public void setPosition(Position pos)
	{
		this.position = pos;
	}

	@Override
	public boolean displayHealth()
	{
		return false;
	}

	@Override
	public float getHealthPercent()
	{
		return 100;
	}

	@Override
	public void onAttacked(Character enemy)
	{
		this.map.remove(this);
	}

	@Override
	public Texture getTexture()
	{
		return null;
	}

	@Override
	public Texture getAnimationKeyFrame(float animationStateTime)
	{
		return animation.getKeyFrame(animationStateTime, true);
	}

	public int getAmount()
	{
		return amount;
	}
}
