package com.sanisimov.myrpg.items;

import com.sanisimov.myrpg.util.Position;

import java.util.Random;

/**
 * Created by sanis on 24.09.2017.
 */
public class ExpFactory
{
	public static ExpOrb getOrb(Position pos){
		Random rng = new Random();
		ExpOrb orb = new ExpOrb(rng.nextInt(100)+100);
		orb.setPosition(pos);
		return orb;
	}
}
