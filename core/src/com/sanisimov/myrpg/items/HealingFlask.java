package com.sanisimov.myrpg.items;

import com.sanisimov.myrpg.util.TextureStorage;
import com.sanisimov.myrpg.characters.Character;

/**
 * Created by sanis on 23.09.2017.
 */
public class HealingFlask extends Flask
{
	HealingFlask(){
		animation = TextureStorage.getAnimation("flask_hp");

		this.name = "Hp flask";
	}
	@Override
	public void use(Character user)
	{
		user.heal(20);
	}
}
