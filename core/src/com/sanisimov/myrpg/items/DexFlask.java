package com.sanisimov.myrpg.items;

import com.sanisimov.myrpg.util.TextureStorage;
import com.sanisimov.myrpg.characters.Character;

/**
 * Created by sanis on 23.09.2017.
 */
public class DexFlask extends Flask
{
	DexFlask(){
		animation = TextureStorage.getAnimation("flask_dex");
		this.name = "Dex flask";
	}

	@Override
	public void use(Character user)
	{
		user.stats.dex += 3;
	}
}
