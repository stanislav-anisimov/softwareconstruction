package com.sanisimov.myrpg.items;

import com.sanisimov.myrpg.util.Position;

import java.util.Random;

/**
 * Created by sanis on 23.09.2017.
 */
public class FlaskFactory
{
	public static Flask randomFlask(Position pos)
	{
		Random rng = new Random();
		Flask flask;
		switch (rng.nextInt(3))
		{
			case 0:
				flask = new HealingFlask();
				flask.setPosition(pos);
				return flask;
			case 1:
				flask = new StrFlask();
				flask.setPosition(pos);
				return flask;
			case 2:
				flask = new DexFlask();
				flask.setPosition(pos);
				return flask;
			default:
				return null;
		}
	}
}
