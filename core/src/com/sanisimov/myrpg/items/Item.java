package com.sanisimov.myrpg.items;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.sanisimov.myrpg.IPlaceable;
import com.sanisimov.myrpg.util.Position;
import com.sanisimov.myrpg.Zone;
import com.sanisimov.myrpg.characters.Character;

/**
 * Created by sanis on 23.09.2017.
 */
public abstract class Item implements IPlaceable
{
	protected String name = "Item";
	protected Zone map = null;
	protected Position position = null;
	protected Texture texture = null;
	Animation<Texture> animation = null;

	@Override
	public void setMap(Zone map)
	{
		this.map = map;
	}

	@Override
	public Zone getMap()
	{
		return map;
	}

	@Override
	public Position getPosition()
	{
		return position;
	}

	@Override
	public void setPosition(Position pos)
	{
		this.position = pos;
	}

	@Override
	public boolean displayHealth()
	{
		return false;
	}

	@Override
	public float getHealthPercent()
	{
		return 0;
	}

	@Override
	public void onAttacked(Character enemy)
	{
		map.remove(this);
	}

	@Override
	public Texture getTexture()
	{
		return texture;
	}

	@Override
	public Texture getAnimationKeyFrame(float animationStateTime)
	{
		return animation.getKeyFrame(animationStateTime, true);
	}

	public abstract void use(Character user);

	@Override
	public String toString(){
		return name;
	}
}
