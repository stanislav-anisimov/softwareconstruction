package com.sanisimov.myrpg.util;

/**
 * Created by sanis on 22.09.2017.
 */
public enum Direction
{
	NORTH,
	SOUTH,
	EAST,
	WEST
}
