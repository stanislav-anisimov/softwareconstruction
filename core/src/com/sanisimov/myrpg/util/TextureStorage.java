package com.sanisimov.myrpg.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

public class TextureStorage {
    private static HashMap<String, Animation<Texture>> animationMap = new HashMap<String, Animation<Texture>>();
    public static Animation<Texture> getAnimation(String placeable){
        return animationMap.get(placeable);
    }
    public static void init(){
        Array<Texture> frames;
        //player
        frames = new Array<Texture>();
        frames.add(new Texture(Gdx.files.internal("player/player_attack_0.png")));
        animationMap.put("player", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
        //player attack
        frames = new Array<Texture>();
        frames.add(new Texture(Gdx.files.internal("player/player_attack_0.png")));
        frames.add(new Texture(Gdx.files.internal("player/player_attack_1.png")));
        frames.add(new Texture(Gdx.files.internal("player/player_attack_2.png")));
        frames.add(new Texture(Gdx.files.internal("player/player_attack_3.png")));
        animationMap.put("player_attack", new Animation<Texture>(2f, frames, Animation.PlayMode.NORMAL));
        //monster
        frames = new Array<Texture>();
        frames.add(new Texture((Gdx.files.internal("monster_0.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_1.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_2.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_3.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_4.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_5.png"))));
        frames.add(new Texture((Gdx.files.internal("monster_6.png"))));
        animationMap.put("monster", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
        //dex flask
        frames = new Array<Texture>();
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_0.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_1.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_2.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_3.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_4.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_5.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_6.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/dex/DexFlask_7.png"))));
        animationMap.put("flask_dex", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
        //str flask
        frames = new Array<Texture>();
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_0.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_1.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_2.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_3.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_4.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_5.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_6.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/str/StrFlask_7.png"))));
        animationMap.put("flask_str", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
        //healing flask
        frames = new Array<Texture>();
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_0.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_1.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_2.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_3.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_4.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_5.png"))));
        frames.add(new Texture((Gdx.files.internal("flasks/hp/HpFlask_6.png"))));
        animationMap.put("flask_hp", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
        //exp
        frames = new Array<Texture>();
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_0.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_1.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_2.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_3.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_4.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_5.png"))));
        frames.add(new Texture((Gdx.files.internal("exp/ExpOrb_6.png"))));
        animationMap.put("orb_exp", new Animation<Texture>(1f, frames, Animation.PlayMode.LOOP));
    }

}
