package com.sanisimov.myrpg.util;

import java.util.ArrayList;

/**
 * Created by sanis on 21.09.2017.
 */
public class Position
{
	public Position(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	private int x = 0;
	private int y = 0;

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public Position move(Direction direction)
	{
		switch (direction)
		{
			case NORTH:
				return new Position(x, y + 1);
			case SOUTH:
				return new Position(x, y - 1);
			case EAST:
				return new Position(x + 1, y);
			case WEST:
				return new Position(x - 1, y);
			default:
				return null;
		}
	}

	public boolean equals(Position other)
	{
		return this.x == other.getX() && this.y == other.getY();
	}

	@Override
	public String toString(){
		return String.valueOf(x) + ";" + String.valueOf(y);
	}

	public boolean near(Position other){
		if(this.move(Direction.NORTH).equals(other)
				|| this.move(Direction.EAST).equals(other)
				|| this.move(Direction.SOUTH).equals(other)
				|| this.move(Direction.WEST).equals(other)){
			return true;
		}
		return false;
	}

	public ArrayList<Position> getNeighbours(){
		ArrayList<Position> res = new ArrayList<Position>();
		res.add(this.move(Direction.NORTH));
		res.add(this.move(Direction.NORTH).move(Direction.EAST));
		res.add(this.move(Direction.EAST));
		res.add(this.move(Direction.EAST).move(Direction.SOUTH));
		res.add(this.move(Direction.SOUTH));
		res.add(this.move(Direction.SOUTH).move(Direction.WEST));
		res.add(this.move(Direction.WEST));
		res.add(this.move(Direction.WEST).move(Direction.NORTH));
		return res;
	}
}
