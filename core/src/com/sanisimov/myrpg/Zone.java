package com.sanisimov.myrpg;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sanisimov.myrpg.characters.Character;
import com.sanisimov.myrpg.characters.Player;
import com.sanisimov.myrpg.items.ExpOrb;
import com.sanisimov.myrpg.items.Item;
import com.sanisimov.myrpg.util.Direction;
import com.sanisimov.myrpg.util.Position;
import javafx.util.Pair;

import java.util.ArrayList;

import static java.lang.Math.floor;

public class Zone
{
	Zone(int x, int y)
	{
		this.setXlen(x);
		this.setYlen(y);
		inventory = new Texture(Gdx.files.internal("inventory.png"));
	}

	private ArrayList<IPlaceable> map = new ArrayList<IPlaceable>();
	private int Xlen;
	private int Ylen;
	private int cellSize;
	private float lineThickness;
	private Player player;
	float animationStateTime = 0;
	Texture inventory;
	Position bottomLeft;

	public void setBottomLeft(Position center){
		bottomLeft = new Position(center.getX() - this.Xlen * cellSize / 2, center.getY() - this.Ylen * cellSize / 2);
	}

	public int getCellSize()
	{
		return cellSize;
	}

	public void setCellSize(int cellSize)
	{
		this.cellSize = cellSize;
	}

	public float getLineThickness()
	{
		return lineThickness;
	}

	public void setLineThickness(float lineThickness)
	{
		this.lineThickness = lineThickness;
	}


	public void render(SpriteBatch batch, Texture pen, Position center, BitmapFont font)
	{
		batch.setColor(Color.BLACK);
		for (int i = 0; i < this.getXlen() + 1; i++)
		{
			batch.draw(pen, bottomLeft.getX() + i * cellSize, bottomLeft.getY(), lineThickness, this.getYlen() * cellSize);
		}
		for (int i = 0; i < this.getYlen() + 1; i++)
		{
			batch.draw(pen, bottomLeft.getX(), bottomLeft.getY() + i * cellSize, this.getXlen() * cellSize, lineThickness);
		}

		batch.setColor(Color.WHITE);

		//draw placeables
		for (IPlaceable obj : map)
		{
			Texture tx;
			//draw icon
			if (obj.getAnimationKeyFrame(animationStateTime) != null)
			{
				animationStateTime += Gdx.graphics.getDeltaTime();
				tx = obj.getAnimationKeyFrame(animationStateTime);
			} else
			{
				tx = obj.getTexture();
			}

			Position pos = obj.getPosition();
			batch.draw(tx, bottomLeft.getX() + pos.getX() * cellSize, bottomLeft.getY() + pos.getY() * cellSize, cellSize, cellSize);
			//draw health bar
			if (obj.displayHealth())
			{
				//draw current health
				batch.setColor(1, 0, 0, 1);
				batch.draw(pen,
						bottomLeft.getX() + pos.getX() * cellSize + cellSize * 0.1f,
						bottomLeft.getY() + pos.getY() * cellSize + cellSize * 0.9f,
						cellSize * 0.8f * obj.getHealthPercent(),
						lineThickness * 2);
				//draw missing health
				batch.setColor(1, 0, 0, 0.3f);
				batch.draw(pen,
						bottomLeft.getX() + pos.getX() * cellSize + cellSize * 0.1f + cellSize * 0.8f * obj.getHealthPercent(),
						bottomLeft.getY() + pos.getY() * cellSize + cellSize * 0.9f,
						cellSize * 0.8f * (1 - obj.getHealthPercent()),
						lineThickness * 2);
				batch.setColor(Color.WHITE);
			}
		}


		font.setColor(Color.BLACK);

		//draw exp
		font.draw(batch, "Level: " + String.valueOf(player.stats.level.level), bottomLeft.getX() + this.Xlen * cellSize / 2f, bottomLeft.getY() - cellSize / 2f);

		batch.setColor(0, 0, 30, 1);
		batch.draw(pen,
				bottomLeft.getX() - cellSize / 2,
				bottomLeft.getY() - cellSize,
				cellSize * (this.Xlen + 1) * player.stats.level.getExpPercent(),
				lineThickness * 3);
		batch.setColor(0, 0, 30, 0.3f);
		batch.draw(pen,
				bottomLeft.getX() - cellSize / 2 + cellSize * (this.Xlen + 1) * player.stats.level.getExpPercent(),
				bottomLeft.getY() - cellSize,
				cellSize * (this.Xlen + 1) * (1 - player.stats.level.getExpPercent()),
				lineThickness * 3);
		batch.setColor(Color.WHITE);
		//draw inventory
		batch.draw(inventory,
				bottomLeft.getX() + this.Xlen * cellSize + cellSize,
				bottomLeft.getY() + (this.Ylen * cellSize - cellSize * 8),
				cellSize * 4,
				cellSize * 8);
		Pair<Float, Float> inventoryListTopLeft = new Pair<Float, Float>(bottomLeft.getX() + this.Xlen * cellSize + cellSize * 1.2f, bottomLeft.getY() + this.Ylen * cellSize - cellSize * 0.5f);
		ArrayList<Item> inventory = player.getInventory();
		int offset = 0;
		for (Item item : inventory)
		{
			font.draw(batch, item.toString() + "    [" + String.valueOf(offset + 1) + "]",
					inventoryListTopLeft.getKey(),
					inventoryListTopLeft.getValue() - offset * cellSize * 0.7f);
			offset++;
		}
		//draw stats
		font.draw(batch, "STR: " + String.valueOf(player.stats.str) + "     DEX: " + String.valueOf(player.stats.dex) +
						"\n     HP: " + String.valueOf(player.currentHp) + "/" + String.valueOf(player.totalHp),
				bottomLeft.getX() + this.Xlen * cellSize + cellSize,
				bottomLeft.getY() + cellSize * 1.8f);
	}

	public int getXlen()
	{
		return Xlen;
	}

	public void setXlen(int xlen)
	{
		Xlen = xlen;
	}

	public int getYlen()
	{
		return Ylen;
	}

	public void setYlen(int ylen)
	{
		Ylen = ylen;
	}

	public boolean add(IPlaceable obj)
	{
		if (checkBounds(obj.getPosition()) && getPlaceable(obj.getPosition()) == null)
		{
			obj.setMap(this);
			map.add(obj);
			return true;
		}
		return false;
	}

	public boolean moveCharacter(Character character, Direction direction)
	{
		if(!character.canMove()){
			return true;
		}
		Position newPos = character.getPosition().move(direction);
		if (checkBounds(newPos))
		{
			IPlaceable placeable = getPlaceable(newPos);
			if (placeable == null)
			{
				character.setPosition(newPos);
			} else if (placeable instanceof Item)
			{
				character.setPosition(newPos);
				character.pickUp((Item) placeable);
				map.remove(placeable);
			} else if (placeable instanceof ExpOrb)
			{
				character.setPosition(newPos);
				character.addExp(((ExpOrb) placeable).getAmount());
				map.remove(placeable);
			}
			return true;
		}
		return false;
	}

	private boolean validatePosition(Position pos)
	{
		if (!(pos != null && pos.getX() >= 0 && pos.getX() < Xlen && pos.getY() >= 0 && pos.getY() < Ylen))
		{
			return false;
		}
		for (IPlaceable obj : map)
		{
			if (pos.equals(obj.getPosition()))
			{
				if (!(obj instanceof Item))
				{
					return false;
				}
			}
		}
		return true;
	}

	private boolean checkBounds(Position pos)
	{
		if (!(pos != null && pos.getX() >= 0 && pos.getX() < Xlen && pos.getY() >= 0 && pos.getY() < Ylen))
		{
			return false;
		}
		return true;
	}

	public Player getPlayer()
	{
		return player;
	}

	public void setPlayer(Player player)
	{
		this.player = player;
	}

	public void onClick(Vector3 coords, Position center)
	{
		Position bottomLeft = new Position(center.getX() - this.Xlen * cellSize / 2, center.getY() - this.Ylen * cellSize / 2);
		Position clickedPosition = new Position((int) floor((coords.x - bottomLeft.getX()) / cellSize), (int) floor((coords.y - bottomLeft.getY()) / cellSize));
		if (!checkBounds(clickedPosition))
			return;
		if (clickedPosition.near(player.getPosition()) && getPlaceable(clickedPosition) != null)
		{
			player.attackStart(getPlaceable(clickedPosition));
		}
	}

	private IPlaceable getPlaceable(Position pos)
	{
		for (IPlaceable obj : map)
		{
			if (pos.equals(obj.getPosition()))
			{
				return obj;
			}
		}
		return null;
	}

	public void remove(IPlaceable obj)
	{
		map.remove(obj);
	}
}
